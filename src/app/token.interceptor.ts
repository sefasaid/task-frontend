import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from './services/api.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    public snackBar: MatSnackBar,
    public apiService: ApiService
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let headers = new HttpHeaders();
    headers = request.headers;
    this.apiService.isLoadingSubject.next(true);

    if (localStorage.getItem('token') != null && (!request.url.startsWith('https://task-api-bucket') && !request.url.startsWith('https://api.spoonacular.com'))) {
      const token = localStorage.getItem('token');
      headers = new HttpHeaders().set("Authorization", 'Bearer ' + token);
    }
    const AuthRequest = request.clone({ headers: headers });
    return next.handle(AuthRequest)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMsg = '';

          if (!!error.error.error) {
            errorMsg = `Error: ${error.error.error}`;
          } else {
            errorMsg = `Error : ${error.error}`;
          }
          this.apiService.isLoadingSubject.next(false)
          return throwError(errorMsg);
        }),
        tap((event) => {
          if (event instanceof HttpResponse) {
            this.apiService.isLoadingSubject.next(false)
          }
        })
      )
  }
}
