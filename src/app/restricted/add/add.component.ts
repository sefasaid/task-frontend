import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  public preview = 'assets/media/upload.jpeg'
  fileToUpload: File | null = null;
  title: string = '';
  fat: number;
  calories: number;
  isLoading$: Observable<boolean>;
  editMode = false;
  imageName: string = '';
  id: string;
  carbohydrates: number;
  constructor(
    private apiService: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.isLoading$ = this.apiService.isLoadingSubject;
    const id = this.route.snapshot.params['id']
    if (id) {
      this.id = id;
      this.editMode = true;
      this.getIngredient(id);
    }

  }
  getIngredient(id: string) {
    this.apiService.getIngredient(id).subscribe((res: any) => {
      this.preview = res.Item.imageUrl;
      this.title = res.Item.title;
      this.fat = res.Item.fat;
      this.calories = res.Item.calories;
      this.carbohydrates = res.Item.carbohydrates;
      this.imageName = res.Item.imageName;
    })
  }

  ngOnInit(): void {
  }

  update() {
    if (!this.calories || !this.fat || !this.carbohydrates || this.title.trim() === '') {
      this.snackbar.open('All Fields Are Required', 'Ok', {
        duration: 3000
      });
      return;
    }
    let data = {
      imageName: this.imageName,
      calories: this.calories,
      fat: this.fat,
      carbohydrates: this.carbohydrates,
      title: this.title.trim()
    };
    if (this.fileToUpload) {
      this.apiService.upload(this.fileToUpload).subscribe((res: any) => {
        data.imageName = res.photoFilename;
        this.apiService.updateIngredient(this.id, data).subscribe((res: any) => {
          this.snackbar.open('Ingredient Updated', 'Ok', {
            duration: 3000
          });
          this.router.navigate(['/app/ingredients'])
          return;
        })
      }, (err) => {
        this.snackbar.open('Error, Please try again', 'Ok', {
          duration: 3000
        });
        return;
      })
      return;
    }
    this.apiService.updateIngredient(this.id, data).subscribe((res: any) => {
      this.snackbar.open('Ingredient Updated', 'Ok', {
        duration: 3000
      });
      this.router.navigate(['/app/ingredients'])
      return;
    })

  }

  save() {
    if (this.editMode) {
      this.update();
      return;
    }
    if (!this.calories || !this.fat || !this.carbohydrates || !this.fileToUpload || this.title.trim() === '') {
      this.snackbar.open('All Fields Are Required', 'Ok', {
        duration: 3000
      });
      return;
    }
    this.apiService.upload(this.fileToUpload).subscribe((res: any) => {
      const data = {
        imageName: res.photoFilename,
        calories: this.calories,
        fat: this.fat,
        carbohydrates: this.carbohydrates,
        title: this.title.trim()
      };
      this.apiService.add(data).subscribe((res: any) => {

        this.snackbar.open('Ingredient Added', 'Ok', {
          duration: 3000
        });
        this.router.navigate(['/app/ingredients'])
        return;
      })
    }, (err) => {

      this.snackbar.open('Error, Please try again', 'Ok', {
        duration: 3000
      });
      return;
    })

  }
  getFromApi() {
    if (this.title.trim() !== '') {
      this.apiService.ingredientSearch(this.title.trim())
        .subscribe((res: any) => {
          res.nutrition.nutrients.map((nutrient) => {
            if (nutrient.name === 'Calories') this.calories = nutrient.amount;
            if (nutrient.name === 'Fat') this.fat = nutrient.amount;
            if (nutrient.name === 'Carbohydrates') this.carbohydrates = nutrient.amount;
          })
        }, (err) => {
          this.snackbar.open('Not Found', 'Ok', {
            duration: 3000
          });
        })
    } else {
      this.snackbar.open('Title Required', 'Ok', {
        duration: 3000
      });
    }
  }
  fileChangeEvent(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.fileToUpload = file;

    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file);
  }
}
