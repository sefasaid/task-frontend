import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements OnInit {

  isLoading = true;
  public page = 1;
  public maxPage = 1;
  public maxCount = 0;
  public perPage = 0;
  // I know not best way I need to create models but time is limited
  public ingredients: any[] = [];
  constructor(
    private apiService: ApiService
  ) {
    this.loadData();
  }
  loadData() {
    this.apiService.getIngredients(this.page)
      .subscribe((res: any) => {
        this.maxPage = Math.ceil(res.Count / res.PerPage);
        this.maxCount = res.Count;
        this.ingredients = res.Items;
        this.perPage = res.PerPage;
      })
  }
  pages() {
    return Array.from({ length: this.maxPage }, (_, i) => i + 1)
  }
  changePage(page: number) {
    this.page = page;
    this.loadData()
  }
  delete(index: number) {
    this.apiService.deleteIngredient(this.ingredients[index].id).subscribe((res: any) => {
      this.loadData();
    })
  }

  ngOnInit(): void {
  }

}
