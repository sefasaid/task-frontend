import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { RestrictedComponent } from './restricted.component';

const routes: Routes = [
  {
    path: '',
    component: RestrictedComponent,
    children: [

      {
        path: 'ingredients',
        component: IngredientsComponent,
      },
      {
        path: 'ingredients/add',
        component: AddComponent,
      },
      {
        path: 'ingredients/add/:id',
        component: AddComponent,
      },
      { path: '', redirectTo: 'ingredients', pathMatch: 'full' },
      { path: '**', redirectTo: 'ingredients', pathMatch: 'full' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestrictedRoutingModule { }
