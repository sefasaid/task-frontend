import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestrictedRoutingModule } from './restricted-routing.module';
import { RestrictedComponent } from './restricted.component';
import { IngredientsComponent } from './ingredients/ingredients.component';

import { InlineSVGModule } from 'ng-inline-svg';
import { AddComponent } from './add/add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RestrictedComponent,
    IngredientsComponent,
    AddComponent,
  ],
  imports: [
    CommonModule,
    RestrictedRoutingModule,
    InlineSVGModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class RestrictedModule { }
