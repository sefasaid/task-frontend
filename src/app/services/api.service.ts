import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService implements CanActivate {
  public isLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
  }
  public canActivate(): boolean {
    const token = localStorage.getItem('token');
    if (!token) {
      this.router.navigate(['/auth'])
    }
    return !!token
  }
  login(credentials: { email: string, password: string }) {
    return this.http.post(environment.url + 'users/login', credentials)
      .pipe(map((data: any) => {
        if (data.errorMessage)
          return throwError(data.errorMessage);
        localStorage.setItem('token', data.token);
        return of(data);
      }), switchMap(
        data => data
      ))
  }

  upload(image) {
    console.log(image);
    return this.http.post(environment.url + 'upload', { fileName: image.name }).pipe(
      map((data: any) => {
        if (data.errorMessage)
          return throwError(data.errorMessage);
        return of(data);
      }), switchMap(
        data => data
      ),
      switchMap((data: any) => {
        return this.http.put(data.uploadURL, image, { headers: { 'Content-Type': image.type, 'x-amz-acl': 'public-read' } }).pipe(map(putData => data))
      })
    )
  }
  add(data) {
    this.isLoadingSubject.next(true);
    return this.http.post(environment.url + 'ingredients', data).pipe(map((data: any) => {
      if (data.errorMessage)
        return throwError(data.errorMessage);
      return of(data);
    }), switchMap(
      data => data
    ))
  }
  ingredientSearch(title: string) {
    return this.http.get(environment.spoonacularApi + 'food/ingredients/search?query=' + title + '&apiKey=' + environment.spoonacularApiKey)
      .pipe(
        map((data: any) => {
          if (!!data.results[0]) {
            return of(data.results[0])
          }
          return throwError('Not Found');
        }),
        switchMap((data: any) => data),
        switchMap((data: any) => {
          return this.http.get(environment.spoonacularApi + 'food/ingredients/' + data.id + '/information?amount=100&unit=g&apiKey=' + environment.spoonacularApiKey);
        }))
  }

  getIngredients(page?: number) {
    let url = environment.url + 'ingredients'
    if (page) {
      url += '?page=' + page;
    }
    return this.http.get(url)
      .pipe(map((data: any) => {
        if (data.errorMessage)
          return throwError(data.errorMessage);
        return of(data);
      }), switchMap(
        data => data
      ))
  }
  deleteIngredient(id: string) {
    return this.http.delete(environment.url + 'ingredients/' + id).pipe(map((data: any) => {
      if (data.errorMessage)
        return throwError(data.errorMessage);
      return of(data);
    }), switchMap(
      data => data
    ))
  }
  getIngredient(id: string) {
    return this.http.get(environment.url + 'ingredients/' + id).pipe(map((data: any) => {
      if (data.errorMessage)
        return throwError(data.errorMessage);
      return of(data);
    }), switchMap(
      data => data
    ))
  }
  updateIngredient(id: string, data) {
    return this.http.put(environment.url + 'ingredients/' + id, data).pipe(map((data: any) => {
      if (data.errorMessage)
        return throwError(data.errorMessage);
      return of(data);
    }), switchMap(
      data => data
    ))
  }
}
